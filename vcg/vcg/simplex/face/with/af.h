#ifndef __VCGLIB_FACE_AF_TYPE
#define __VCGLIB_FACE_AF_TYPE

#define FACE_TYPE FaceAF 

#define __VCGLIB_FACE_AF

#include <vcg/simplex/face/base.h> 

#undef FACE_TYPE 

#undef __VCGLIB_FACE_AF

#endif
