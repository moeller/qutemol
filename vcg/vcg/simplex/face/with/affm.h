#ifndef __VCGLIB_FACE_AFFM_TYPE
#define __VCGLIB_FACE_AFFM_TYPE

#define FACE_TYPE FaceAFFM 

#define __VCGLIB_FACE_AF
#define __VCGLIB_FACE_FM

#include <vcg/simplex/face/base.h> 

#undef FACE_TYPE 

#undef __VCGLIB_FACE_AF
#undef __VCGLIB_FACE_FM

#endif
