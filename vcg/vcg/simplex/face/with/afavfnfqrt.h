#ifndef __VCGLIB_FACE_AFAVFNFQRT_TYPE
#define __VCGLIB_FACE_AFAVFNFQRT_TYPE

#define FACE_TYPE FaceAFAVFNFQRT

#define __VCGLIB_FACE_AF
#define __VCGLIB_FACE_FN
#define __VCGLIB_FACE_AV
#define __VCGLIB_FACE_FQ
#define __VCGLIB_FACE_RT

#include <vcg/simplex/face/base.h> 

#undef FACE_TYPE 

#undef __VCGLIB_FACE_AF
#undef __VCGLIB_FACE_FN
#undef __VCGLIB_FACE_AV
#undef __VCGLIB_FACE_FQ
#undef __VCGLIB_FACE_RT
#endif