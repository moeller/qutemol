#ifndef __VCGLIB_FACE_FMFN_TYPE
#define __VCGLIB_FACE_FMFN_TYPE

#define FACE_TYPE FaceFMFN 

#define __VCGLIB_FACE_FM
#define __VCGLIB_FACE_FN

#include <vcg/simplex/face/base.h> 

#undef FACE_TYPE 

#undef __VCGLIB_FACE_FM
#undef __VCGLIB_FACE_FN

#endif