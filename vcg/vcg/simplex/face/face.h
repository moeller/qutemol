/****************************************************************************
* VCGLib                                                            o o     *
* Visual and Computer Graphics Library                            o     o   *
*                                                                _   O  _   *
* Copyright(C) 2004                                                \/)\/    *
* Visual Computing Lab                                            /\/|      *
* ISTI - Italian National Research Council                           |      *
*                                                                    \      *
* All rights reserved.                                                      *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *   
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License (http://www.gnu.org/licenses/gpl.txt)          *
* for more details.                                                         *
*                                                                           *
****************************************************************************/
/****************************************************************************
  History

$Log: face.h,v $
Revision 1.3  2004/10/22 11:11:46  cignoni
uppercase error in FACE_TYPE

Revision 1.2  2004/07/15 11:31:06  ganovelli
overwritten previous version (it was a copy of base.h)

First commit...

****************************************************************************/

#ifndef __VCGLIB__BASICFACETYPE
#define __VCGLIB__BASICFACETYPE

#define FACE_TYPE Face
#include <vcg/simplex/face/base.h>

#undef FACE_TYPE
#endif
