#ifndef __VCGLIB_TETRA_TN_TYPE
#define __VCGLIB_TETRA_TN_TYPE

#define TETRA_TYPE TetraTN 

#define __VCGLIB_TETRA_TN

#include <vcg/simplex/tetrahedron/base.h> 

#undef TETRA_TYPE 

#undef __VCGLIB_TETRA_TN

#endif
